package com.cash.online.exam.domain.repository;

import com.cash.online.exam.domain.model.Loan;
import com.cash.online.exam.domain.model.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long> {
    List<Loan> findByUser(User user);

    int deleteLoanByUser(User user);

    @Query("select l from LOANS l where :user_id is null or l.user.id = :user_id")
    Page<Loan> findAllByUser(@Param("user_id") Long user_id, Pageable pageable);
}

