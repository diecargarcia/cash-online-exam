package com.cash.online.exam.domain.service;

import com.cash.online.exam.api.dto.LoanDTO;
import com.cash.online.exam.api.dto.PageDTO;
import com.cash.online.exam.api.dto.request.LoanSearchRequest;
import com.cash.online.exam.api.dto.response.LoanResponse;
import com.cash.online.exam.domain.model.Loan;
import com.cash.online.exam.domain.model.User;
import com.cash.online.exam.domain.repository.LoanRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.PageRequest.of;

@Service
@Slf4j
public class LoanService {

    @Autowired
    private LoanRepository loanRepository;

    public List<LoanDTO> findLoanByUser(User user) {
        List<LoanDTO> loans = null;

        log.info("Find user with id: {}", user.getId());
        var loanModel = this.loanRepository.findByUser(user);
        if (loanModel != null) {
            log.info("Find the loans for the userId: {}", user.getId());
            loans = loanModel.parallelStream().map(l -> new LoanDTO(l.getId(), l.getTotal(), user.getId())).collect(
                    toList());
        }

        return loans;
    }

    public int deleteLoanByUserId(User user) {
        return this.loanRepository.deleteLoanByUser(user);
    }

    public LoanResponse findLoan(LoanSearchRequest request) {
        var response = new LoanResponse();
        var page = of(request.getPage(), request.getSize());
        log.info("Find the loans for the userId: {}, page: {}:{}", request.getUserId(), request.getPage(), request.getSize());
        var loans = this.loanRepository.findAllByUser(request.getUserId(), page);

        if (loans != null) {
            response.setPaging(new PageDTO(loans.getNumber(), loans.getSize(), loans.getTotalElements()));
            List<LoanDTO> loansDTO =
                    loans.getContent().parallelStream().map(l -> new LoanDTO(l.getId(), l.getTotal(), l.getUser().getId()))
                         .collect(toList());

            response.setItems(loansDTO);
            log.info("{} loans were found.", loansDTO.size());
        }

        return response;
    }
}
