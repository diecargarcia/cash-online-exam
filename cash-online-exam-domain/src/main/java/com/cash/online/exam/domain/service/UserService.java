package com.cash.online.exam.domain.service;

import com.cash.online.exam.api.dto.request.AddUserRequest;
import com.cash.online.exam.api.dto.response.UserResponse;
import com.cash.online.exam.domain.model.User;
import com.cash.online.exam.domain.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import javax.transaction.Transactional;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LoanService loanService;

    public UserResponse saveUser(AddUserRequest request) {
        var user = new User();
        user.setEmail(request.getEmail());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        var persistedUser = userRepository.save(user);
        log.info("The user with identification {} was saved", persistedUser.getId());
        return new UserResponse(user.getId(), user.getEmail(), user.getFirstName(), user.getLastName(), null);
    }

    @Transactional
    public Boolean deleteUser(Long id) {
        var user = this.userRepository.findById(id);
        if (user.isPresent()) {
            var loan = this.loanService.deleteLoanByUserId(user.get());
            log.info("{} loans were deleted", loan);
            this.userRepository.delete(user.get());
            log.info("Delete the userId: {}", id);
            return true;
        }
        return false;

    }

    public Optional<UserResponse> findUserById(Long id) {
        return this.userRepository.findById(id)
                                  .map(u -> new UserResponse(u.getId(), u.getEmail(),
                                                             u.getFirstName(),
                                                             u.getLastName(),
                                                             this.loanService.findLoanByUser(u)));

    }
}
