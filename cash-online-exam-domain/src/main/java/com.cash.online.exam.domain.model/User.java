package com.cash.online.exam.domain.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.validation.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import static java.util.Calendar.getInstance;
import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.TemporalType.TIMESTAMP;


@Entity(name = "USERS")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "user_id_seq")
    @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq")
    @Column(name = "ID")
    private Long id;
    @NonNull()
    @Column(name = "EMAIL", unique = true, length = 150, nullable = false)
    @Email
    private String email;
    @NonNull
    @Column(name = "FIRST_NAME", length = 20, nullable = false)
    private String firstName;
    @NonNull
    @Column(name = "LAST_NAME", length = 20, nullable = false)
    private String lastName;
    @Temporal(TIMESTAMP)
    @Column(name = "REGIST_DATE", nullable = false)
    private Calendar registDate;

    @PrePersist
    protected void onCreate() {
        this.setRegistDate(getInstance());
    }
}
