package com.cash.online.exam.domain.model;

import org.hibernate.annotations.OnDelete;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.OnDeleteAction.CASCADE;


@Entity(name = "LOANS")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Loan {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "loan_id_seq")
    @SequenceGenerator(name = "loan_id_seq", sequenceName = "loan_id_seq")
    @Column(name = "ID")
    private Long id;
    @NonNull()
    @Column(name = "TOTAL", nullable = false)
    //TODO se podría considerar valores minimo y máximo
    private BigDecimal total;
    @NonNull
    @ManyToOne(fetch = LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = CASCADE)
    private User user;
}
