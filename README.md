## CASH ONLINE EXAM
Examen para cash online.

## Información General
Se expone una API REST de usuarios y préstamos

## Contacto

* Diego García: [diecargarcia@gmail.com](mailto:diecargarcia@gmail.com)
	
## Tecnología
Esta aplicación fue creada con:

* Java 11
* H2
* lombok
* Spring boot
* Jetty

## Componentes de Solución

Es una aplicación desarrollada en Java. Dentro de su arquitectura, posee una base de datos en H2, la cual se utilizará para almacenar los usuarios y préstamos.

Para esta solución se utilizó JETTY embebido.

## IDE
* Instalación de Lombok [aqui](https://www.baeldung.com/lombok-ide)
	
## Setup
Para correr esta aplicación localmente:

```
$ cd ../cash-online-exam
$ mvn clean install
$ cd cash-online-exam-service
$ mvn spring-boot:run
```

## Estructura del proyecto
El proyecto consta de los siguientes modulos:

* cash-online-exam-api: API de la aplicación.
* cash-online-exam-domain: Servicios de negocio.
* cash-online-exam-service: Servicios REST

## API (swagger)

La documentación de la API: [aqui](http://localhost:8080/swagger-ui.html)

