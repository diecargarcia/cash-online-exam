package com.cash.online.exam.service.repository;

import com.cash.online.exam.domain.model.User;
import com.cash.online.exam.domain.repository.UserRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

import static java.util.Calendar.getInstance;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository repository;

    @Test
    public void saveUser() {
        User user = new User();
        user.setRegistDate(getInstance());
        user.setLastName("Garcia");
        user.setFirstName("Diego");
        user.setEmail("diecargarcia@gmail.com");
        entityManager.persistAndFlush(user);
        assertThat(user.getId()).isNotNull();
    }

    @Test(expected = ConstraintViolationException.class)
    public void saveUserWithError() {
        User user = new User();
        user.setRegistDate(getInstance());
        user.setLastName("Garcia");
        user.setFirstName("Diego");
        user.setEmail("diecargarcia");
        entityManager.persistAndFlush(user);
    }

    @Test
    public void deleteUser() {
        User user = new User();
        user.setRegistDate(getInstance());
        user.setLastName("Garcia");
        user.setFirstName("Diego");
        user.setEmail("diecargarcia@gmail.com");
        entityManager.persistAndFlush(user);
        assertThat(repository.findById(user.getId())).isNotEmpty();

        repository.delete(user);
        assertThat(repository.findById(user.getId())).isEmpty();
    }
}