package com.cash.online.exam.service.controller;

import com.cash.online.exam.api.dto.request.AddUserRequest;
import com.cash.online.exam.domain.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.boot.test.json.JacksonTester.initFields;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController controllerUnderTest;

    private MockMvc mockMvc;

    private JacksonTester<AddUserRequest> jsonAddUser;

    @Before
    public void setup() {
        initMocks(this);
        this.mockMvc = standaloneSetup(controllerUnderTest).build();
        initFields(this, new ObjectMapper());
    }

    @Test
    public void getUser() throws Exception {
        mockMvc.perform(get("/users/12")).andExpect(status().isNotFound());
    }

    @Test
    public void createUser() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(
                post("/users/").contentType(APPLICATION_JSON).content(
                        jsonAddUser.write(new AddUserRequest("diecargarcia@gmail.com", "Diego", "Garcia")).getJson()
                                                                     )).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(CREATED.value());
    }

}

