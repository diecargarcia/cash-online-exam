package com.cash.online.exam.service.repository;

import com.cash.online.exam.domain.model.Loan;
import com.cash.online.exam.domain.model.User;
import com.cash.online.exam.domain.repository.LoanRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import javax.persistence.PersistenceException;

import static java.util.Calendar.getInstance;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.data.domain.PageRequest.of;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LoanRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private LoanRepository repository;

    @Test
    public void saveLoan() {
        User user = new User();
        user.setRegistDate(getInstance());
        user.setLastName("Garcia");
        user.setFirstName("Diego");
        user.setEmail("diecargarcia@gmail.com");

        entityManager.persistAndFlush(user);
        assertThat(user.getId()).isNotNull();

        Loan loan = new Loan();
        loan.setTotal(BigDecimal.valueOf(100));
        loan.setUser(user);

        entityManager.persistAndFlush(loan);
        assertThat(loan.getId()).isNotNull();
    }

    @Test(expected = PersistenceException.class)
    public void saveLoanWithError() {
        User user = new User();
        user.setRegistDate(getInstance());
        user.setLastName("Garcia");
        user.setFirstName("Diego");
        user.setEmail("diecargarcia@gmail.com");

        entityManager.persistAndFlush(user);
        assertThat(user.getId()).isNotNull();

        Loan loan = new Loan();
        loan.setUser(user);

        entityManager.persistAndFlush(loan);
        assertThat(loan.getId()).isNotNull();
    }

    @Test
    public void deleteLoan() {
        User user = new User();
        user.setRegistDate(getInstance());
        user.setLastName("Garcia");
        user.setFirstName("Diego");
        user.setEmail("diecargarcia@gmail.com");

        entityManager.persistAndFlush(user);
        assertThat(user.getId()).isNotNull();

        Loan loan = new Loan();
        loan.setUser(user);

        repository.deleteAll();
        assertThat(repository.findAll()).isEmpty();
    }

    @Test
    public void findLoanByUser() {
        User user = new User();
        user.setRegistDate(getInstance());
        user.setLastName("Garcia");
        user.setFirstName("Diego");
        user.setEmail("diecargarcia@gmail.com");

        entityManager.persistAndFlush(user);
        assertThat(user.getId()).isNotNull();

        Loan loan = new Loan();
        loan.setTotal(BigDecimal.valueOf(100));
        loan.setUser(user);

        entityManager.persistAndFlush(loan);
        assertThat(loan.getId()).isNotNull();

        assertThat(repository.findByUser(user)).isNotEmpty();
        assertThat(repository.findAllByUser(user.getId(), of(0, 1))).isNotEmpty();
        assertThat(repository.findAllByUser(user.getId(), of(1, 1))).isEmpty();
        assertThat(repository.findAllByUser(2L, of(1, 1))).isEmpty();
    }
}