drop table LOANS;
drop table USERS;

CREATE TABLE USERS(
  ID NUMBER(10,0) NOT NULL PRIMARY KEY,
  EMAIL  VARCHAR(250) NOT NULL UNIQUE,
  FIRST_NAME VARCHAR(250) NOT NULL,
  LAST_NAME VARCHAR(250) NOT NULL,
  REGIST_DATE DATE NOT NULL
);

CREATE TABLE LOANS(
  ID NUMBER(10,0) NOT NULL PRIMARY KEY,
  TOTAL  NUMBER(10,2) NOT NULL,
  USER_ID NUMBER(10,0),
    foreign key (USER_ID) references USERS(ID)
);

CREATE SEQUENCE IF NOT EXISTS user_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE IF NOT EXISTS loan_id_seq START WITH 1 INCREMENT BY 1;

SET @USER_ID_CARLOS = NEXTVAL('user_id_seq');
SET @USER_ID_LAURA = NEXTVAL('user_id_seq');

INSERT INTO USERS (ID, EMAIL, FIRST_NAME, LAST_NAME, REGIST_DATE) VALUES
  (@USER_ID_CARLOS,'carlos@gmail.com', 'Carlos', 'Perez', CURRENT_TIMESTAMP()),
  (@USER_ID_LAURA,'laura@gmail.com', 'Laura', 'Gonzalez', CURRENT_TIMESTAMP()),
  (NEXTVAL('user_id_seq'),'julian@hotmail.com', 'Julian', 'Ben', CURRENT_TIMESTAMP());

INSERT INTO LOANS (ID, TOTAL, USER_ID) VALUES
  (NEXTVAL('loan_id_seq'),500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),1000, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),1500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),2500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),3500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),4500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),5500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),6500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),7500, @USER_ID_CARLOS),
  (NEXTVAL('loan_id_seq'),8500, @USER_ID_CARLOS);

INSERT INTO LOANS (ID, TOTAL, USER_ID) VALUES
  (NEXTVAL('loan_id_seq'),50000, @USER_ID_LAURA),
  (NEXTVAL('loan_id_seq'),100000, @USER_ID_LAURA),
  (NEXTVAL('loan_id_seq'),150000, @USER_ID_LAURA),
  (NEXTVAL('loan_id_seq'),250000, @USER_ID_LAURA),
  (NEXTVAL('loan_id_seq'),350000, @USER_ID_LAURA),
  (NEXTVAL('loan_id_seq'),450000, @USER_ID_LAURA),
  (NEXTVAL('loan_id_seq'),550000, @USER_ID_LAURA);




