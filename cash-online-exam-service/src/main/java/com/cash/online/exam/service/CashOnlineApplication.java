package com.cash.online.exam.service;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@ComponentScan(basePackages = {
        "com.cash.online"})
@EntityScan(basePackages = "com.cash.online.exam.domain.model")
@Slf4j
public class CashOnlineApplication
        implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(CashOnlineApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("CashOnlineApplication -> run has started");
    }
}


