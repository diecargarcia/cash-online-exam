package com.cash.online.exam.service.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestLoggingFilter
        implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        this.doRealFilter((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, filterChain);
    }

    @Override
    public void init(FilterConfig filterConfig) {
        log.info("Creating RequestLoggingFilter.");
    }

    protected void doRealFilter(HttpServletRequest servletRequest, HttpServletResponse servletResponse,
                                FilterChain filterChain) throws IOException, ServletException {
        try {
            Map<String, String> headers = getHeadersIfAvailable(servletRequest);
            String fullUrl = getFullUrl(servletRequest);
            String method = servletRequest.getMethod();

            logToSlf4j(method, fullUrl, headers);
        } catch (Exception ex) {
            log.error("Unexpected error in RequestLoggingFilter, letting request pass through without log", ex);
        } finally {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private void logToSlf4j(String method, String fullUrl, Map<String, String> headers) {
        log.info("{} to: [{}] with headers {}", method, fullUrl, headers);
    }

    @Override
    public void destroy() {
        log.info("Destroying RequestLoggingFilter");
    }

    /**
     * Extracts the request's headers
     */
    private Map<String, String> getHeadersIfAvailable(HttpServletRequest request) {
        Map<String, String> headers = new HashMap<>();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String header = headerNames.nextElement();
            headers.put(header, request.getHeader(header));
        }

        return headers;
    }

    private String getFullUrl(HttpServletRequest request) {
        StringBuilder requestBuilder =
                new StringBuilder(request.getScheme()).append("://").append(request.getServerName())
                                                      .append(":").append(request.getServerPort())
                                                      .append(request.getRequestURI());
        if (request.getQueryString() != null) {
            requestBuilder.append("?").append(request.getQueryString());
        }

        return requestBuilder.toString();

    }
}
