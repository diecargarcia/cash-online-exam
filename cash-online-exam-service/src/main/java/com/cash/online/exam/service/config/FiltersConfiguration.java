package com.cash.online.exam.service.config;

import com.cash.online.exam.service.filter.RequestLoggingFilter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;

import static java.util.EnumSet.of;
import static javax.servlet.DispatcherType.REQUEST;


@Configuration
public class FiltersConfiguration {

    private static final String URL_PATTERN = "/*";
    private static final String REQUEST_LOGGING_FILTER_NAME = "RequestLoggingFilter";

    private static final Map<String, Integer> orderMap;

    static {
        orderMap = new HashMap();
        orderMap.put(REQUEST_LOGGING_FILTER_NAME, 1);
    }

    @Bean
    public FilterRegistrationBean loggingFilterConfiguration() {
        return buildFilterRegistration(new RequestLoggingFilter(), URL_PATTERN,
                                       REQUEST_LOGGING_FILTER_NAME);
    }

    private FilterRegistrationBean buildFilterRegistration(Filter filter, String urlPattern, String filterName) {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(filter);
        registration.addUrlPatterns(urlPattern);
        registration.setName(filterName);
        registration.setDispatcherTypes(of(REQUEST));
        registration.setOrder(orderMap.get(filterName));
        return registration;
    }

}
