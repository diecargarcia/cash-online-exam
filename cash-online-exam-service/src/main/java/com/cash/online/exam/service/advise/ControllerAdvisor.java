package com.cash.online.exam.service.advise;


import com.cash.online.exam.api.dto.response.ApiErrorResponse;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
@Slf4j
public class ControllerAdvisor {

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ApiErrorResponse> handlerBadRequestServerSideException(HttpServletRequest request,
                                                                                 MethodArgumentTypeMismatchException e) {
        var message = isNotEmpty(e.getMessage()) ? e.getMessage() : getRootCauseMessage(e);
        var error = new ApiErrorResponse(400, message);
        log.warn("validation error", e);
        return new ResponseEntity<>(error, BAD_REQUEST);
    }

    @ExceptionHandler({TransactionSystemException.class})
    public ResponseEntity<ApiErrorResponse> handlerConstraintViolationException(HttpServletRequest request,
                                                                                TransactionSystemException e) {
        var error = new ApiErrorResponse(400, "Constraint Violation");
        log.warn("bad request", e);
        return new ResponseEntity<>(error, BAD_REQUEST);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<ApiErrorResponse> handlerIntegrityViolationException(HttpServletRequest request,
                                                                               DataIntegrityViolationException e) {
        var error = new ApiErrorResponse(400, "Data Integrity Violation");
        log.warn("bad request", e);
        return new ResponseEntity<>(error, BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorResponse> handlerGenericServerSideException(HttpServletRequest request, Exception e) {
        var message = isNotEmpty(e.getMessage()) ? e.getMessage() : getRootCauseMessage(e);
        var error = new ApiErrorResponse(500, message);
        log.error("unexpected error", e);
        return new ResponseEntity<>(error, INTERNAL_SERVER_ERROR);
    }

}



