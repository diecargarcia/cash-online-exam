package com.cash.online.exam.service.controller;

public class QueryParam {
    public static final String PAGE = "page";
    public static final String SIZE = "size";
    public static final String USER_ID = "user_id";
}

