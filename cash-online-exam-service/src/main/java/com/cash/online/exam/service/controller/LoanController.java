package com.cash.online.exam.service.controller;

import com.cash.online.exam.api.dto.request.LoanSearchRequest;
import com.cash.online.exam.api.dto.response.LoanResponse;
import com.cash.online.exam.domain.service.LoanService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import static com.cash.online.exam.service.controller.QueryParam.PAGE;
import static com.cash.online.exam.service.controller.QueryParam.SIZE;
import static com.cash.online.exam.service.controller.QueryParam.USER_ID;
import static com.cash.online.exam.service.controller.URL.LOANS;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@Validated()
@RequestMapping(value = {LOANS})
@Api(value = "Loans microservice", description = "This API has a queries for loans")
public class LoanController {

    @Autowired
    private LoanService loanService;

    @GetMapping
    @ApiOperation(value = "Search for loans", notes = "Returns paginated loans.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error (uncaught exception)")})
    public ResponseEntity<LoanResponse> getUserById(
            @RequestParam(value = PAGE) int page,
            @RequestParam(value = SIZE) int size,
            @RequestParam(value = USER_ID, required = false) Long userId) {
        var response = this.loanService.findLoan(new LoanSearchRequest(page, size, userId));

        return ok(response);
    }
}
