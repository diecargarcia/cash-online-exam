package com.cash.online.exam.service.controller;

import com.cash.online.exam.api.dto.request.AddUserRequest;
import com.cash.online.exam.api.dto.response.UserResponse;
import com.cash.online.exam.domain.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.NonNull;

import static com.cash.online.exam.service.controller.URL.USERS;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = {USERS})
@Api(value = "Users microservice", description = "This API has a CRUD for users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Find an user", notes = "Return a user by Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error (uncaught exception)")})
    public ResponseEntity<UserResponse> getUserById(@NonNull @PathVariable Long id) {
        var user = this.userService.findUserById(id);

        return user.isPresent() ? ok(user.get()) : notFound().build();
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Delete an user", notes = "Delete a user by Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error (uncaught exception)")})
    public ResponseEntity<Void> deleteUserById(@NonNull @PathVariable Long id) {
        var status = this.userService.deleteUser(id);
        return (status ? ok() : notFound()).build();
    }

    @PostMapping()
    @ApiOperation(value = "Create an user", notes = "Create a new user")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error (uncaught exception)")})
    public ResponseEntity<UserResponse> saveUser(@NonNull @RequestBody AddUserRequest userRequest) {
        var user = this.userService.saveUser(userRequest);
        return new ResponseEntity(user, CREATED);
    }
}
