package com.cash.online.exam.api.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanDTO {
    @NonNull
    private Long id;
    @NonNull
    private BigDecimal total;
    @NonNull
    private Long userId;
}
