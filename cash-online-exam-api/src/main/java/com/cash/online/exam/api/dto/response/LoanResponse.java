package com.cash.online.exam.api.dto.response;

import com.cash.online.exam.api.dto.LoanDTO;
import com.cash.online.exam.api.dto.PageDTO;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanResponse {
    private List<LoanDTO> items;
    private PageDTO paging;
}
