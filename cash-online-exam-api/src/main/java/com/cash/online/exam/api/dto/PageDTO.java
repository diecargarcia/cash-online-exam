package com.cash.online.exam.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageDTO {
    @NonNull
    private int page;
    @NonNull
    private int size;
    @NonNull
    private long total;
}
