package com.cash.online.exam.api.dto.response;

import com.cash.online.exam.api.dto.LoanDTO;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private List<LoanDTO> loans;
}